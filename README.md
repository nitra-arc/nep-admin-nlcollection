# NlCollectionBundle

## Описание

данный бандл предназначен для:

* Добавления поля типа коллекции;
 * Этот тип поля используется для визуализации "коллекции" некоторого поля или формы. В простейшем смысле, это может быть массив текстовых полей, которые заполняют поле массива писем. В более сложных примерах, вы можете вставлять целые формы, что полезно при создании формы, которые предоставляют один-ко-многим (например, продукта, откуда можно управлять многими похожих фотографий продукта).

## Подключение

* **composer.json**

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-admin-nlcollectionbundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\NlCollectionBundle\NitraNlCollectionBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

## Использование

* **generator.yml**

```yml
products:
    label:          fields.products.label
    formType:       nlcollection
    addFormOptions:
        type:               genemu_autocomplete
        options:
            autocomplete_class: NitraProductBundle:Product
        allow_add:          true
        allow_delete:       true
        delete_empty:       true
        removing_message:   'удалить?'
```

* **опции**
 * **type** - тип поля которое будет использоваться(text, choice и т.д.). Например если будет использоваться массив e-mail то нужно использовать тип поля email.
 * **allow_add** - булевая опция. Если true то включена возможность добавления элементов.
 * **allow_delete** - булевая опция. Если true то включена возможность удаления элементов.
 * **delete_empty** - **Доступно только начиная с Symfony 2.5.** Если вы хотите удалить пустые записи из формы вы должны установить этот параметр на true. Тем не менее, существующие записи будут удалены, только если у вас включен параметр allow_delete. В противном случае пустые значения будут сохранены.
 * **removing_message** - вывод сообщения подтвержедния удаления.
 * **options** - массив опций. например если использвать тип поля choice то поле options будет иметь примерно такой вид:

```yml
    'type'   => 'choice',
    'options'  => array(
        'choices'  => array(
            'nashville' => 'Nashville',
            'paris'     => 'Paris',
            'berlin'    => 'Berlin',
            'london'    => 'London',
        ),
    ),
```