<?php

namespace Nitra\NlCollectionBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class NlCollectionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['allow_delete']     = $options['allow_delete'];
        $view->vars['removing_message'] = $options['removing_message'];
        $view->vars['callbacks']        = $options['callbacks'];
        $view->vars['prototype_name']   = $options['prototype_name'];
        $view->vars['add_label']        = $options['add_label'];
        $view->vars['widget_type']      = $options['widget_type'];
        $view->vars['theads']           = $options['theads'];
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'collection';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'nlcollection';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);

        $resolver->setDefaults(array(
            'removing_message'  => null,
            'add_label'         => 'add',
            'widget_type'       => 'ul',
            'theads'            => array(),
            'callbacks'         => array(
                'post_remove'            => null,
                'pre_remove'             => null,
                'post_add'               => null,
                'pre_add'                => null,
                'pre_append_delete_link' => null,
            ),
        ));
    }
}